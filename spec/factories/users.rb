# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    sequence :email do
      |n| "email#{n}@helabs.com.br" 
    end
    name "Mollis Ullamcorper"
    password '12345678'
  end
end
