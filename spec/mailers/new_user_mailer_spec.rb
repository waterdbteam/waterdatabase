require 'rails_helper'

RSpec.describe NewUserMailer do
  describe 'user_sing_up_email' do
    let(:user) { FactoryGirl.build(:user) }
    let(:mail) { NewUserMailer.user_sing_up_email(user) }

    it 'renders the subject' do
      expect(mail.subject).to eql("Um usuário se registrou")
    end

    it 'renders the receiver email' do
      expect(mail.to).to eql(['contato@waterdb.com.br'])
    end
    
    it 'renders the sender email' do
      expect(mail.from).to eql(['contato@waterdb.com.br'])
    end

    it 'assigns @name' do
      expect(mail.body.encoded).to match(user.name)
    end

    it 'assigns @email' do
      expect(mail.body.encoded).to match(user.email)
    end
  end
end
