require 'rails_helper'

RSpec.describe ApplicationController, :type => :controller do

  controller do
    before_filter :authenticate_admin_user!, only: [:index]

    def index
      @current_user = current_user
      render text: 'Hello World'
    end
  end

  describe '#authenticate_admin_user!' do
    context 'not logged in' do
      before do
        get :index
      end

      it { should redirect_to(root_path) }
      it { should set_the_flash.to('Apenas para administradores...') }
    end

    context 'as a normal user' do
      let(:user) { create(:user)}
      before do
        sign_in(user)
        get :index
      end

      it { should redirect_to(root_path) }
      it { should set_the_flash.to('Apenas para administradores...') }
    end

    context 'as a admin user' do
      let(:admin) { create(:user, is_admin: true)}
      before do
        sign_in(admin)
        get :index
      end

      it { should respond_with(:success) }
    end
  end

  describe '#user_expired' do
    let(:user) { create(:user, expiration_date: 1.day.ago) }
    before do
      sign_in(user)
      get :index
    end

    it{ expect(controller.user_signed_in?).to be_falsey }
  end

  describe '#user_blocked' do
    let(:user) { create(:user, blocked: true) }
    before do
      sign_in(user)
      get :index
    end

    it{ expect(controller.user_signed_in?).to be_falsey }
  end

  describe 'helpers' do
    describe "is_admin?" do
      context "admin user" do
        let!(:user) { create(:user, email: 'foo1@bar.com', is_admin: true) }

        before do
          expect(controller).to receive(:current_user).and_return(user)
        end

        it { expect(controller.is_admin?).to be true }
      end

      context "normal user" do
        let!(:user) { create(:user, email: 'lorem@bar.com') }

        before do
          expect(controller).to receive(:current_user).and_return(user)
        end

        it { expect(controller.is_admin?).to be false }
      end
    end
  end
end
