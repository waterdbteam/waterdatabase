require 'rails_helper'

RSpec.describe CategoriesController, :type => :controller do

  describe "GET index" do
    let!(:category) { create(:category) }

    def action
      get :index
    end

    context "signed in" do
      login_user
      before { action }

      it { expect(response).to have_http_status(:success) }
      it { expect(assigns(:categories)).to match_array(category) }
      it { is_expected.to render_template(:index) }
    end

    context 'user expired' do
      let(:user) { create(:user, expiration_date: 1.day.ago) }
      before do
        sign_in(user)
        action
      end

      it { should redirect_to(new_user_session_path) }
      it { should set_the_flash.to(I18n.t('application.account_expired')) }
    end

    context 'user blocked' do
      let(:user) { create(:user, blocked: true) }
      before do
        sign_in(user)
        action
      end

      it { should redirect_to(new_user_session_path) }
      it { should set_the_flash.to(I18n.t('application.account_blocked')) }
    end
  end
end
