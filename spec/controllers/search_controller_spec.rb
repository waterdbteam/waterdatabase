require 'rails_helper'

RSpec.describe SearchController, :type => :controller do
  describe "GET #index" do
    login_user
    let(:content) { create(:content) }

    def action
      get :index, q: content.title[1,3]
    end

    it 'search with success' do
      expect(Content).to receive(:search).with(content.title[1,3]).and_call_original
      action
    end
  end
end
