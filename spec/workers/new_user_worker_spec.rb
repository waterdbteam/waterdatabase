require 'rails_helper'

RSpec.describe NewUserWorker do
  let(:user) { FactoryGirl.create(:user) }
  let(:delay) { double('delay').as_null_object }

  describe 'Send mail' do
    it 'enqueues mail worker' do
      NewUserWorker.perform_async(user.id)

      expect(NewUserWorker).to have_enqueued_job(user.id)
    end
    it 'call mailer' do
      allow(NewUserMailer).to receive(:delay).and_return(delay)

      delay.should_receive(:user_sing_up_email).with(user.id)
      NewUserWorker.new.perform(user.id)
    end
  end
end
