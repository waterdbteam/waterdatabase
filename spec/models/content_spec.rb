require 'rails_helper'

RSpec.describe Content, :type => :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:locale) }
    it { is_expected.to validate_presence_of(:category) }
    it { is_expected.to ensure_length_of(:title).is_at_most(255) }
    it { is_expected.to ensure_length_of(:locale).is_at_most(255) }
  end

  describe "associations" do
    it { is_expected.to belong_to(:category) }
    it { is_expected.to have_many(:documents).dependent(:destroy) }
  end

  describe "#locale" do
    it "has the current locale by default" do
      expect(subject.locale).to eq(I18n.locale)
    end
  end

  describe "#search" do
    let!(:contents) { create_list(:content, 2, title: 'Specificatíon') }

    it "find contents" do
      expect(Content.search('Spec')).to match_array(contents)
    end

    it "find contents with accent" do
      expect(Content.search('Specificatíon')).to match_array(contents)
    end

    it "find contents case insensitivity" do
      expect(Content.search('spec')).to eq(contents)
    end

    it "be empty when does not find anything" do
      expect(Content.search('loa')).to be_empty
    end

    it 'does search with special characters' do
      expect(Content.search('tion')).to match_array(contents)
    end
  end
end
