require 'rails_helper'

RSpec.describe User, :type => :model do

  describe 'validations' do
    context 'presence' do
      it { is_expected.to validate_presence_of(:name) }
      it { is_expected.to validate_presence_of(:email) }
    end
    context 'uniqueness' do
      let!(:user) { FactoryGirl.create(:user) }
      it { is_expected.to validate_uniqueness_of(:email) }
    end
  end

  describe 'user blocked after create' do
    let(:user) { create(:user) }
    it { expect(user.blocked).to be_falsey }
  end

  describe "#send_email_admin" do
    it 'call mailer after create user' do
      expect(NewUserWorker).to receive(:perform_async).with(kind_of(Integer))
      user = create(:user)
      user.run_callbacks(:commit)
    end
  end

  describe 'expiration_date' do
    let(:user) { FactoryGirl.create(:user) }

    it 'create user with expiration_date to 7 dayss' do
      expect(user.expiration_date).to eq(7.days.from_now.to_date)
    end

    it 'update expiration_date' do
      user.expiration_date = 30.days.from_now
      user.save

      expect(user.expiration_date).to eq(30.days.from_now.to_date)
    end
  end
end
