require 'rails_helper'

RSpec.describe 'SignUp', type: :request do
  it "does sign up a user" do
    visit root_path
    click_link('Cadastre-se')

    fill_in('user_name', :with => 'Pedro Ferreira')
    fill_in('user_email', :with => 'pedro@example.com')
    fill_in('user_password', :with => '12345678')
    fill_in('user_password_confirmation', :with => '12345678')
    fill_in('user_phone', :with => '11 98766 2345')

    click_on('Cadastrar')

    expect(page).to have_content('Login efetuado com sucesso.')
  end
end
