require 'rails_helper'

RSpec.describe 'AccountUpdate', type: :request do
  let(:user) { create(:user) }  
  it "does update account" do
    login_as(user)

    visit edit_user_registration_path

    click_on('Enviar')

    expect(page).to have_content("Sua conta foi atualizada com sucesso.")
  end
end
