class SearchController < ApplicationController
  before_action :authenticate_user!

  def index
    @contents = Content.search(params[:q])
  end
end
