class CategoriesController < ApplicationController
  before_filter :authenticate_user!

  def index
    @categories = Category.order(:position)
  end
end
