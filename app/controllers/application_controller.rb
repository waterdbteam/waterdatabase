class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  ensure_security_headers # See more: https://github.com/twitter/secureheaders
  before_action :set_locale
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :user_expired
  before_action :user_blocked

  helper_method :is_admin?

  def default_url_options(options={})
    { :locale => ((I18n.locale == I18n.default_locale) ? nil : I18n.locale) }
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def is_admin?
    current_user.is_admin?
  end

  def ensure_admin!
    redirect_to(root_path, notice: I18n.t('application.ensure_admin')) unless is_admin?
  end

  def authenticate_admin_user!
    unless user_signed_in? && current_user.is_admin?
      redirect_to(root_url, notice: 'Apenas para administradores...')
    end
  end

  def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password, :password_confirmation, :phone, :company, :position, area_interest: []) }
      devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :password, :password_confirmation, :phone, :company, :position, area_interest: []) }
  end

  def user_expired
    if user_signed_in? && current_user.expiration_date < Time.now.to_date && !current_user.is_admin?
      flash[:notice] = I18n.t('application.account_expired')
      sign_out current_user
    end
  end

  def user_blocked
    if user_signed_in? && current_user.blocked?
      flash[:notice] = I18n.t('application.account_blocked')
      sign_out current_user
    end
  end
end
