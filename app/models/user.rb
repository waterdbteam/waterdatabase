class User < ActiveRecord::Base
  after_commit :send_admin_email, on: :create
  before_validation  :set_expiration_date
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :name, :email, :expiration_date, presence: true
  validates :email, uniqueness: true

  enum position: [:director, :adviser, :manager, :supervisor, :operational]
  AREA_INTERESTS = [:supply, :sewage, :drainage, :residue, :management]

  private
  def send_admin_email
    NewUserWorker.perform_async(self.id)
  end

  def set_expiration_date
    self.expiration_date ||= 7.days.from_now
  end
end
