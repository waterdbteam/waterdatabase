class Content < ActiveRecord::Base
  validates :title, :locale, :category, presence: true
  validates :title, :locale, length: { maximum: 255 }
  belongs_to :category
  has_many :documents, dependent: :destroy

  default_scope -> { where(locale: I18n.locale) }

  def self.search(q)
    includes(:category).where("unaccent(title) ILIKE unaccent(?)", "%#{q}%")
  end
end
