class NewUserWorker
  include Sidekiq::Worker

  def perform(user_id)
    user = User.find(user_id)
    NewUserMailer.user_sing_up_email(user).deliver
  end
end
