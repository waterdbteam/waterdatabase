class DownloadMailer < ActionMailer::Base
  default from: 'contato@waterdb.com.br'

  def generate_deleted_list_files(file_path)
    attachments['relatorio_arquivos_apagados.xls'] = File.read(file_path)
    mail(cc: 'contato@waterdb.com.br', bcc: 'rodrigo@rtoledo.inf.br', subject: "Relatório com arquivos apagados do sistema")
  end
end
