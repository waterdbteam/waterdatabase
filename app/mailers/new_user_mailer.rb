class NewUserMailer < ActionMailer::Base
  default from: 'contato@waterdb.com.br'

  def user_sing_up_email(user)
    @user = user
    mail(to: 'contato@waterdb.com.br', subject: "Um usuário se registrou")
  end
end
