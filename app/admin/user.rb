ActiveAdmin.register User do
  filter :name
  filter :email
  filter :created_at
  filter :phone
  filter :phone
  filter :company
  filter :area_interest
  filter :position
  filter :expiration_date
  filter :is_admin
  filter :blocked
  filter :premium


  index do
    column :id
    column :name
    column :email
    column :created_at
    column :phone
    column :phone
    column :company
    column :area_interest
    column :position
    column :expiration_date
    column :is_admin
    column :blocked
    column :premium
    actions
  end

  form do |f|
    f.semantic_errors
    f.inputs 'Dados' do
      f.input :name
      f.input :email
      f.input :created_at
      f.input :phone
      f.input :company
      f.input :area_interest, as: :check_boxes, collection: area_interest_collection
      f.input :position, as: :select, collection: position_collection
      f.input :expiration_date
      f.input :is_admin
      f.input :premium
      f.input :blocked
      f.actions
    end
  end


  permit_params :name, :email, :created_at, :phone, :phone, :company, :position, :is_admin, :expiration_date, :blocked, :premium, area_interest: []
end
