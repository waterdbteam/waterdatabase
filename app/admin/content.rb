ActiveAdmin.register Content do
  permit_params :title, :description, :locale, :premium, :category_id


  form do |f|
    f.inputs 'Dados' do
      f.input :title
      f.input :description
      f.input :category
      f.input :locale, as: :radio, collection: ['pt-BR', 'es']
      f.input :premium
    end
    f.actions
  end
end
