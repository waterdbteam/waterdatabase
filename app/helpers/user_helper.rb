module UserHelper

 def position_collection
   result = {}
   User.positions.keys.each { |k| result.merge!(I18n.t("users.position.#{k}") => k) }
   result
 end

 def area_interest_collection
  User::AREA_INTERESTS.map { |k| I18n.t("users.area_interest.#{k}") }
 end
end
