Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  devise_for :users, controllers: {registrations: "users/registrations"}

  get :search, to: 'search#index'
  get :contact, to: 'pages#contact'
  scope "(:locale)" do
    get 'pages/home'

    resources :categories, only: [:show, :index] do
      resources :contents do
        resources :documents, only: [:destroy, :create]
      end
    end

    root 'pages#home'
  end

  get '/:locale' => 'categories#index'
end
