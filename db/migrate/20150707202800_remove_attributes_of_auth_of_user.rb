class RemoveAttributesOfAuthOfUser < ActiveRecord::Migration
  def change
    remove_column :users, :google_uid
    remove_column :users, :google_token
    remove_column :users, :oauth_expires_at
  end
end
