class AddPremiumToContent < ActiveRecord::Migration
  def change
    add_column :contents, :premium, :boolean, default: false, null: false
  end
end
