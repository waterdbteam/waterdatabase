class RenameNameToSlugFromCategories < ActiveRecord::Migration
  def up
    rename_column :categories, :name, :slug
    remove_column :categories, :locale
  end
end
