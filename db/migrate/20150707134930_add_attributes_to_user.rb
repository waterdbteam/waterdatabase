class AddAttributesToUser < ActiveRecord::Migration
  def change
    add_column :users, :phone, :string
    add_column :users, :company, :string
    add_column :users, :area_interest, :text, array: true, default: []
    add_column :users, :position, :integer, defaul: 0
  end
end
