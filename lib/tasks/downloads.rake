require 'spreadsheet'
require 'securerandom'
namespace :downloads do
  desc "Generate CSV with deleted files in environment"
  task generate_deleted_list_files: :environment do
    book  = Spreadsheet::Workbook.new
    sheet = book.create_worksheet name: 'Caminhos de arquivos removidos'
    sheet.row(0).concat %w{Categoria Conteudo Arquivo URL}
    Document.where("id < ?",1090).each_with_index do |document,index|
      sheet.row(index+1).push(document.try(:content).try(:category).try(:name), document.try(:content).try(:title), document.file_file_name)
    end
    file_path = Rails.root.join('tmp',"#{SecureRandom.hex}.xls")
    book.write(file_path)
    DownloadMailer.generate_deleted_list_files(file_path).deliver
  end
end